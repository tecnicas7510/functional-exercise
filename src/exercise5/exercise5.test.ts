import { getPrice as newImplementation } from './exercise5';
import { getPrice as oldImplementation } from './original';
import { Category } from './types';

describe('getPrice', function() {
  [Category.One, Category.Two, Category.Five].forEach(c => {
    [true, false].forEach(o => {
      it(`Will produce the same result for category ${c} and offer ${o}`, () => {
        expect(oldImplementation(c, o)).toStrictEqual(newImplementation(c, o));
      });
    });
  });
});
