import { Tree, sumTree, maxTree } from './exercise8';

describe('sumTree', function() {
  it('Sum a leaf tree', () => {
    expect(sumTree(1)).toBeCloseTo(1);
  });

  it('Sum a tree with direct leaves', () => {
    expect(sumTree([1, 2])).toBeCloseTo(3);
  });

  it('Sum a tree with indirect leaves', () => {
    expect(sumTree([1, [2, 3]])).toBeCloseTo(6);
  });
});

describe('maxTree', function() {
  it('Max a leaf tree', () => {
    expect(maxTree(1)).toBeCloseTo(1);
  });

  it('Max a tree with direct leaves', () => {
    expect(maxTree([1, 2])).toBeCloseTo(2);
  });

  it('Max a tree with indirect leaves', () => {
    expect(maxTree([1, [2, 3]])).toBeCloseTo(3);
  });
});
